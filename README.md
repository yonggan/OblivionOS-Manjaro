# OblivionOS | Manjaro Version
This is a Manjaro Spinnoff with minimum changes to achieve a Mac Desktop like Look.
It uses KDE and the WhiteSur Themes available on the KDE Store and Latte Dock.

Official Repository:
[Oblivion Gitea](https://git.oblivioncoding.pro/Yonggan/OblivionOS-Manjaro.git)

!!! Be aware this is highly Alpha !!!
I created this Distro for my own because i sucked to always customize my KDE Desktop

## Downloads
You can find the Downloads of the current Build here:
https://cloud.oblivioncoding.pro/s/DbzpY7oPxd2HJ4B

## Versions:
- kde-whitesur-dark
- kde-whitesur
- kde-whitesur-dark-candy
- kde-whitesur-candy

## Screenshots

### kde-whitesur-dark

![kde-whitesur-dark](images/kde-whitesur-dark.png)

### kde-whitesur

![kde-whitesur](images/kde-whitesur.png)

### kde-whitesur-dark-candy

![kde-whitesur-dark-candy](images/kde-whitesur-dark-candy.png)

### kde-whitesur-candy

![kde-whitesur-candy](images/kde-whitesur-candy.png)

## Contribution
Just go ahead clone the repo and make a Pull Request.
